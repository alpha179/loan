from django.contrib import messages
from django.shortcuts import redirect
from client.models import LoanApplication, LoanConfirmation
from django.views.generic.edit import CreateView
from django.views.generic import ListView
from django.urls import reverse_lazy
from .decorators import GroupRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class DashboardView(LoginRequiredMixin, GroupRequiredMixin, TemplateView):
    template_name = "home/dashboard.html"


class LoanConfirmationCreate(LoginRequiredMixin, GroupRequiredMixin, CreateView):
    model = LoanConfirmation
    fields = ['application_id', 'amount', 'interest_rate', 'loan_copy', 'is_confirm']
    template_name = 'home/form_loan_confirm.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uid'
    success_url = reverse_lazy('home:dashboard')

    def get_initial(self):
        loan = LoanApplication.objects.get(uid=self.kwargs['uuid'])
        initial = {
            'application_id': loan.pk,
            'amount': loan.loan_amount,
            }
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

class LoanAppList(LoginRequiredMixin, GroupRequiredMixin, ListView):
    model = LoanApplication
    template_name = 'home/loanapplication_list.html'
    context_object_name = 'all_loan'

    def get_queryset(self):
        queryset = LoanApplication.objects.all()
        return queryset

class ApprovedList(LoginRequiredMixin, GroupRequiredMixin, ListView):
    model = LoanApplication
    template_name = 'home/loanapproved_list.html'
    context_object_name = 'all_loan'

    def get_queryset(self):
        queryset = LoanApplication.objects.filter(pk__in=LoanConfirmation.objects.all())
        return queryset


def login_success(request):

    if request.user.groups.filter(name="home").exists():
        messages.success(request, f'You have been signed in successfully!')
        return redirect("home:dashboard")
    else:
        messages.success(request, f'You have been signed in successfully!')
        return redirect("client:dashboard")

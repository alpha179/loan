from django.urls import path
from . import views
from django.conf.urls import url

app_name = "home"

urlpatterns = [
    path('dashboard/', views.DashboardView.as_view(), name="dashboard"),
    path('loan-list/', views.LoanAppList.as_view(), name="loan-detail"),
    path('all-loan-list/', views.ApprovedList.as_view(), name="all-loan-detail"),
    url(r'^loan-confirm/(?P<uuid>[^/]+)/$', views.LoanConfirmationCreate.as_view(), name="loan-confirm"),
]

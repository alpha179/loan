from django.urls import path

from . import views


app_name = "client"

urlpatterns = [
    path('dashboard/', views.DashboardView.as_view(), name="dashboard"),
    path('loan-application/', views.LoanAppCreate.as_view(), name="application"),
    path('loan-list/', views.LoanAppList.as_view(), name="loan-detail"),
]

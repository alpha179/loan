from django.db import models
from django.contrib.auth.models import User
import uuid

def validate_file_extension(value):
    if not (value.name.endswith('.pdf') or value.name.endswith('.PDF')):
        raise ValidationError(u'Only pdf file is supported')

def file_size(value):
    limit = 2 * 1024 * 1024
    if value.size > limit:
        raise ValidationError('File too large. Size should not exceed 2 MiB.')

class LoanApplication(models.Model):
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        )
    name = models.CharField(max_length=200)
    pan_number = models.CharField(max_length=20)

    email = models.CharField(max_length=200)
    tel = models.CharField(max_length=200)

    contact_person_1 = models.CharField(max_length=200, null=True, blank=True)
    email_1 = models.CharField(max_length=200, null=True, blank=True)
    tel_1 = models.CharField(max_length=200, null=True, blank=True)

    bank_branch = models.CharField(max_length=100)
    account_no = models.CharField(max_length=30)
    ifsc_code = models.CharField(max_length=20)

    loan_amount = models.CharField(max_length=5)
    period = models.CharField(max_length=100)

    address = models.TextField(max_length=400)

    pan_doc = models.FileField(upload_to='Doc/%Y/%m/%d/', validators=[validate_file_extension, file_size])
    address_doc = models.FileField(upload_to='Doc/%Y/%m/%d/', validators=[validate_file_extension, file_size])
    cancel_cheque_doc = models.FileField(upload_to='Doc/%Y/%m/%d/', validators=[validate_file_extension, file_size])

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.OneToOneField(User, on_delete=models.PROTECT)
    approved_by = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class LoanConfirmation(models.Model):
    uid = models.UUIDField(unique=True, editable=False, default=uuid.uuid4)
    application_id = models.OneToOneField(LoanApplication, on_delete=models.CASCADE)
    amount = models.CharField(max_length=15)
    interest_rate = models.CharField(max_length=15)
    loan_copy = models.FileField(upload_to='AppDoc/%Y/%m/%d/', validators=[validate_file_extension, file_size])
    is_confirm = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.dwp_brn

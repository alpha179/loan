from django.contrib import admin
from . models import LoanApplication, LoanConfirmation

admin.site.register(LoanApplication)
admin.site.register(LoanConfirmation)
# Register your models here.

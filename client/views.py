from . models import LoanApplication
from django.views.generic.edit import CreateView
from django.views.generic import ListView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "home/dashboard.html"


class LoanAppCreate(LoginRequiredMixin, CreateView):
    model = LoanApplication
    fields = ['name', 'pan_number', 'email', 'tel', 'contact_person_1',
              'email_1', 'tel_1', 'bank_branch', 'account_no', 'ifsc_code',
              'loan_amount', 'period', 'address', 'pan_doc', 'address_doc',
              'cancel_cheque_doc']
    template_name = 'client/form_loan.html'
    success_url = reverse_lazy('client:dashboard')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class LoanAppList(LoginRequiredMixin, ListView):
    model = LoanApplication
    template_name = 'client/loanapplication_list.html'
    context_object_name = 'all_loan'

    def get_queryset(self):
        queryset = LoanApplication.objects.filter(created_by=self.request.user)
        return queryset

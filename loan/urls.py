from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('home.urls')),
    path('client/', include('client.urls')),
    path('', auth_views.LoginView.as_view(template_name='client/login.html'), name='login'),
    path('login_success/', views.login_success, name='login_success'),
    path('logout/', auth_views.LogoutView.as_view(template_name='client/logout.html'), name='logout'),
]
